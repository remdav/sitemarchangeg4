<?php

namespace App\Repository;

use App\Entity\CommercialQuote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CommercialQuote|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommercialQuote|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommercialQuote[]    findAll()
 * @method CommercialQuote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommercialQuoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommercialQuote::class);
    }

    // /**
    //  * @return CommercialQuote[] Returns an array of CommercialQuote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommercialQuote
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
