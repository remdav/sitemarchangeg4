<?php

namespace App\Repository;

use App\Entity\QuoteCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method QuoteCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuoteCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuoteCategory[]    findAll()
 * @method QuoteCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuoteCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuoteCategory::class);
    }

    // /**
    //  * @return QuoteCategory[] Returns an array of QuoteCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuoteCategory
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
