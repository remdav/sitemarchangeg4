<?php

namespace App\Repository;

use App\Entity\CommandeLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CommandeLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommandeLine|null findOneBy(array $criteria, array $commandeBy = null)
 * @method CommandeLine[]    findAll()
 * @method CommandeLine[]    findBy(array $criteria, array $commandeBy = null, $limit = null, $offset = null)
 */
class CommandeLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommandeLine::class);
    }

    
     /**
      * @return Product[] Returns an array of Product objects
    */
   
   
    public function findbyIdCl($value): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.commande_id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }
       



    // /**
    //  * @return CommandeLine[] Returns an array of CommandeLine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->commandeBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommandeLine
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
