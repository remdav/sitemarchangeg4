<?php

namespace App\Form;

use App\Entity\CommandeLine;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandeLineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Product_Id', EntityType::class, [
                "class" => \App\Entity\Product::class,
                "choice_label" => "id",
                "label" => "Id du produit"
            ])
            ->add('quantity', TextType::class, [
                "label" => "Quantité souhaité"
            ]) 
            ->add('Commande_Id', EntityType::class, [
                "class" => \App\Entity\Commande::class,
                "choice_label" => "id",
                "label" => "Id du de la commande"
            ])   
            ->add("submit", SubmitType::class, [
                "label" => "Enregistrer"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CommandeLine::class,
        ]);
    }
}
