<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                "label" => "Avatar"
            ])
            ->add('email', EmailType::class, [
                "label" => "Adresse e-mail"
            ])
            ->add('name', TextType::class, [
                "label" => "Nom"
            ])
            ->add('firstname', TextType::class, [
                "label" => "Prénom"
            ])
            ->add('address', TextType::class, [
                "label" => "Adresse postal"
            ])
            ->add('zip_code', NumberType::class, [
                "label" => "Code postal"
            ])
            ->add('city', TextType::class, [
                "label" => "Ville"
            ])
            ->add('birthday', DateType::class, [
                "label" => "Date de naissance"
            ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
