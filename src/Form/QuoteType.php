<?php

namespace App\Form;

use App\Entity\Quote;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name', TextType::class, [
                'label' => "Nom"
            ])
            ->add('Firstname', TextType::class, [
                "label" => "Prénom"
            ])
            ->add('Brand', TextType::class, [
                "label" => "Nom de l'entreprise"
            ])
            ->add('Title', TextType::class, [
                "label" => "Titre de la demande"
            ])
            ->add('Description', TextareaType::class, [
                "label" => "Description"
            ])
            ->add('mail', EmailType::class, [
                "label" => "Email"
            ])
            ->add('PhoneNumber', TelType::class, [
                "label" => "Numéro de téléphone"
            ])
            ->add('quoteCategory', EntityType::class, [
                "class" => \App\Entity\QuoteCategory::class,
                "choice_label" => "title",
                "label" => "Type de produit"
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quote::class,
        ]);
    }
}
