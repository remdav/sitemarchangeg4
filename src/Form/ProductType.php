<?php

namespace App\Form;

use App\Entity\Product;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                "label" => "Nom"
            ])
            ->add('price', IntegerType::class, [
                "label" => "Prix"
            ])
            ->add('stock', IntegerType::class, [
                "label" => "Nombre en stock"
            ])
            ->add('descriptive', CKEditorType::class, [
                "label" => "Description du produit"
            ])
            ->add('Type_Product_Id', EntityType::class, [
                "class" => \App\Entity\ProductType::class,
                "choice_label" => "denominated",
                "label" => "Type de produit"
            ])
            ->add('picture', FileType::class, [
                "label" => "Contenu de l'article"
            ])
            ->add("submit", SubmitType::class, [
                "label" => "Enregistrer"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
