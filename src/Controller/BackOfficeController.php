<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Product;
use App\Entity\Commande;
use App\Entity\CommandeLine;
use App\Entity\Quote;
use App\Entity\QuoteCategory;
use App\Entity\Users;
use App\Form\ArticlesType;
use App\Form\ProductType;
use App\Form\CommandeType;
use App\Form\CommandeLineType;
use App\Form\ProductTypeType;
use App\Entity\Commercial;
use App\Entity\Service;
use App\Form\ServiceType;
use App\Form\QuoteCategoryType;
use App\Form\QuoteType;
use App\Model\Entity\User;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Form\CommercialType;
use Symfony\Component\HttpFoundation\Request;

class BackOfficeController extends AbstractController
{
    /**
     * @Route("/admin", name="backOfficeManage")
     */
    public function backOfficeManage()
    {
        return $this->render('/admin/admin.html.twig', [
            'controller_name' => 'backOfficeManage',
        ]);
    }

    /**
     * @Route("/admin/article", name="backOfficeArticle")
     */
    public function backOfficeArticle() {
        if($this->getUser() === null){
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $allArticles = $this->getDoctrine()->getManager()->getRepository(Articles::class)->findAll();

        return $this->render('backOffice/article/index.html.twig', [
            "allArticles" => $allArticles,
        ]);
    }

    /**
     * @Route("/admin/article/create", name="backOfficeArticleCreate")
     */
    public function backOfficeArticleCreate(Request $request) {
        if($this->getUser() === null){
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $article = new Articles();
        $form = $this->get("form.factory")->createNamed("createArticle", ArticlesType::class, $article);


        if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $article->setCreateBy($this->getUser());
            $em->persist($article);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Article créé avec succès");
            return $this->redirectToRoute("backOfficeArticle");
        }

        return $this->render('backOffice/article/create.html.twig', [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/article/update/{id}", name="backOfficeArticleUpdate")
     */
    public function backOfficeArticleUpdate(Request $request, $id){
        if($this->getUser() === null){
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $article = $this->getDoctrine()->getManager()->getRepository(Articles::class)->find($id);

        if($article !== null) {
            $form = $this->get("form.factory")->createNamed("updatedArticle", ArticlesType::class, $article);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
                $article->setUpdateAt(new \DateTime());
                $article->setUpdateBy($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Article modifé avec succès");
                return $this->redirectToRoute("backOfficeArticle");
            }

            return $this->render('backOffice/article/update.html.twig', [
                "form" => $form->createView(),
            ]);
        } else {
            $this->addFlash('error', 'La ressource n\'a pas été mise à jour car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeArticle');
        }

    }

    /**
     * @Route("/admin/article/delete/{id}", name="backOfficeArticleDelete")
     */
    public function backOfficeArticleDelete(Request $request, $id){
        if($this->getUser() === null){
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $article = $this->getDoctrine()->getManager()->getRepository(Articles::class)->find($id);

        if($article !== null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Article supprimé avec succès");

            return $this->redirectToRoute("backOfficeArticle");
        } else {
            $this->addFlash('error', 'L\'article n\'a pas été supprimé car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeArticle');
        }
    }

    /**
     * @Route("/user/shippingOrder", name="backofficeShippingOrder")
     */
    public function backofficeShippingOrder()
    {
        return $this->render('backOffice/shippingOrder/index.html.twig', [
            'controller_name' => 'backofficeShippingOrder',
        ]);
    }

    /**
     * @Route("/admin/product", name="backofficeProduct")
     */
    public function backofficeProduct()
    {
        $products = $this->getDoctrine()->getManager()->getRepository(Product::class)->findAll();

        return $this->render('backOffice/product/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/admin/product/create", name="backofficeProductCreate")
     */
    public function backofficeProductCreate(Request $request)
    {
        if($this->getUser() === null){
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $product = new Product();
        $form = $this->get("form.factory")->createNamed("createdProduct", ProductType::class, $product);

        if($request->isMethod("post") && $form->handleRequest($request)->isValid() ) {
            $em = $this->getDoctrine()->getManager();
            $product->setCreateBy($this->getUser());
            $em->persist($product);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Votre produit a bien été enregisté");
            return $this->redirectToRoute("backofficeProduct");
        }

        return $this->render('backOffice/product/create.html.twig', [
            "form" => $form->createView(),
        ]);

    }

    /**
     * @Route("/admin/product/update/{id}", name="backofficeProductUpdate")
     */
    public function backofficeProductUpdate(Request $request, $id) {
        if($this->getUser() === null){
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        if($product !== null) {
            $form = $this->get("form.factory")->createNamed("updateProduct", ProductType::class, $product);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
                $product->setUpdateBy($this->getUser());
                $product->setUpdateAt(new \DateTime());
                 $em->persist($product);
                 $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Le produit a été créé avec succès");
                 return $this->redirectToRoute("backofficeProduct");
            }

            return $this->render('backOffice/product/update.html.twig', [
                "form" => $form->createView(),
            ]);
        } else {
            $this->get("session")->getFlashBag()->add("errror", "Merci de saisir une ressources existante !");
            return $this->redirectToRoute("backofficeProduct");
        }
    }

    /**
     * @Route("/admin/product/delete/{id}", name="backofficeProductDelete")
     */
    public function backofficeProductDelete(Request $request, $id)
    {
        if ($this->getUser() === null) {
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        if($product !== null) {
            $em->remove($product);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "La suppression c'est correctement passée");
            return $this->redirectToRoute("backofficeProduct");
        }

        $this->get("session")->getFlashBag()->add("errorr", "Cette ressource n'existe pas !");
        return $this->redirectToRoute("backofficeProduct");

    }

    /**
     * @route("/admin/product-type/", name="backofficeProductType")
     */
    public function backofficeProductType() {
        $productTypes = $this->getDoctrine()->getManager()->getRepository(\App\Entity\ProductType::class)->findAll();

        return $this->render("backOffice/productType/index.html.twig", [
            "productTypes" => $productTypes,
        ]);
    }

    /**
     * @route("/admin/product-type/create", name="backofficeProductTypeCreate")
     */
    public function backofficeProductTypeCreate(Request $request) {
        if ($this->getUser() === null) {
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $em = $this->getDoctrine()->getManager();
        $productType = new \App\Entity\ProductType();
        $form = $this->get("form.factory")->createNamed("createTypeProduct", ProductTypeType::class, $productType);

        if($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $productType->setCreateBy($this->getUser());
            $em->persist($productType);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type a été correctement créé");
            return $this->redirectToRoute("backofficeProductType");
        }

        return $this->render("backOffice/productType/create.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @route("/admin/product-type/udpate/{id}", name="backofficeProductTypeUpdate")
     */
    public function backofficeProductTypeUpdate(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $productType = $em->getRepository(\App\Entity\ProductType::class)->find($id);

        if($productType !== null) {
            $form = $this->get("form.factory")->createNamed("updateProductType", ProductTypeType::class, $productType);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
                $productType->setCreateBy($this->getUser());

                $em->persist($productType);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Le type a été mise à jour");
                return $this->redirectToRoute("backofficeProductType");
            }

            return $this->render("backOffice/productType/update.html.twig", [
                "form" => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/admin/product-type/delete/{id}", name="backofficeProductTypeDelete")
     */
    public function backofficeProductTypeDelete(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $productType = $em->getRepository(\App\Entity\ProductType::class)->find($id);

        if ($productType !== null) {
            $em->remove($productType);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type a été supprimé");
            return $this->redirectToRoute("backofficeProductType");
        }

        $this->get("session")->getFlashBag()->add("error", "Cette ressource n'existe pas !");
        return $this->redirectToRoute("backofficeProductType");
    }

    /**
     * @Route("/admin/commercial", name="backOfficeCommercial")
     */
    public function backOfficeCommercial()
    {
        $allCommercials = $this->getDoctrine()->getManager()->getRepository(Commercial::class)->findAll();
        return $this->render('backOffice/commercial/index.html.twig', [
            'allCommercials' => $allCommercials,
        ]);
    }

    /**
     * @Route("/admin/commercial/create", name="backOfficeCommercialCreate")
     */
    public function backOfficeCommercialCreate(Request $request)
    {
        $commercial = new Commercial();
        $form = $this->get("form.factory")->createNamed("createCommercial",CommercialType::class,$commercial);

        if($request->isMethod("post") && $form->handleRequest($request)->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
           // $commercial->setCreateBy($this->getUser());
            $em->persist($commercial);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Commercial créé avec succès");
            return $this->redirectToRoute("backOfficeCommercial");
        }
            return $this->render('backOffice/commercial/create.html.twig', [
                "form" => $form->createView(),
            ]);
        
    }

    
    /**
     * @Route("/admin/commercial/update/{id}", name="backOfficeCommercialUpdate")
     */
    public function backOfficeCommercialUpdate(Request $request, $id){
        $commercial = $this->getDoctrine()->getManager()->getRepository(Commercial::class)->find($id);

        if($commercial !== null) {
            $form = $this->get("form.factory")->createNamed("updatedCommercial",CommercialType::class, $commercial);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
                //$commercial->setUpdateAt(new \DateTime());
                //$commercial->setUpdateBy($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($commercial);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Commercial modifé avec succès");
                return $this->redirectToRoute("backOfficeCommercial");
            }

            return $this->render('backOffice/commercial/update.html.twig', [
                "form" => $form->createView(),
            ]);
        } else {
            $this->addFlash('error', 'La ressource n\'a pas été mise à jour car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeCommercial');
        }
    }

    /**
     * @Route("/admin/commercial/delete/{id}", name="backOfficeCommercialDelete")
     */
    public function backOfficeCommercialDelete(Request $request, $id){
        $commercial = $this->getDoctrine()->getManager()->getRepository(Commercial::class)->find($id);

        if($commercial !== null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commercial);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Commercial supprimé avec succès");

            return $this->redirectToRoute("backOfficeCommercial");
        } else {
            $this->addFlash('error', 'Le commercial n\'a pas été supprimé car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeCommercial');
        }
    }

    /**
     * @Route("/admin/quote/create", name="backOfficeQuoteCreate")
     */
    public function backOfficeQuoteCreate(Request $request) {
        $quoteCategory = new QuoteCategory();
        $form = $this->get("form.factory")->createNamed("createQuoteCategory", QuoteCategoryType::class, $quoteCategory);

        if($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $quoteCategory->setCreateAt(new \DateTime());
            $quoteCategory->setCreateBy($this->getUser());
            $em->persist($quoteCategory);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type de catégorie a été créé");
            return $this->redirectToRoute("backOfficeQuote");
        }

        return $this->render('backOffice/quoteCategory/add.html.twig', [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/quote/update/{id}", name="backOfficeQuoteUpdate")
     */
    public function backOfficeQuoteUpdate(Request $request, $id) {
        $quoteCategory = $this->getDoctrine()->getManager()->getRepository(QuoteCategory::class)->find($id);

        if($quoteCategory !== null) {
            $form = $this->get("form.factory")->createNamed("updatedQuote",QuoteCategoryType::class, $quoteCategory);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
                $quoteCategory->setUdpateAt(new \DateTime());
                $quoteCategory->setUpdateBy($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($quoteCategory);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Le type de catégorie a été créé modifier");
                return $this->redirectToRoute("backOfficeQuote");
            }

            return $this->render('backOffice/quoteCategory/update.html.twig', [
                "form" => $form->createView(),
            ]);

        } else {
            $this->addFlash('error', 'La ressource n\'a pas été mise à jour car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeQuote');
        }
    }

    /**
     * @Route("/admin/quote/delete/{id}", name="backOfficeQuoteDelete")
     */
    public function backOfficeQuoteDelete(Request $request, $id){
        $quoteCategory = $this->getDoctrine()->getManager()->getRepository(QuoteCategory::class)->find($id);

        if($quoteCategory !== null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($quoteCategory);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type de catégorie a été supprimé avec succès");

            return $this->redirectToRoute("backOfficeQuote");
        } else {
            $this->addFlash('error', 'Le type n\'a pas été supprimé car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeQuote');
        }
    }

    /**
     * @Route("/admin/quote", name="backOfficeQuote")
     */
    public function backOfficeQuote()
    {
        $allQuotes = $this->getDoctrine()->getManager()->getRepository(QuoteCategory::class)->findAll();

        return $this->render('backOffice/quoteCategory/index.html.twig', [
            'allQuotes' => $allQuotes,
        ]);
    }

    /**
     * @Route("/user/quote/my-quote", name="backOfficeMyQuote")
     */
    public function backOfficeMyQuote()
    {
        $allMyQuotes = $this->getDoctrine()->getManager()->getRepository(Quote::class)->findByUserId($this->getUser());

        return $this->render('backOffice/quote/index.html.twig', [
            'allMyQuotes' => $allMyQuotes,
        ]);
    }

    /**
         * @Route("/admin/quote/request", name="backOfficeQuoteRequest")
     */
    public function backOfficeQuoteRequest()
    {
        $allQuotes = $this->getDoctrine()->getManager()->getRepository(QuoteCategory::class)->findAll();

        return $this->render('backOffice/quoteCategory/index.html.twig', [
            'allQuotes' => $allQuotes,
        ]);
    }

    /**
     * @Route("/company/quote/mail", name="backOfficeQuoteMail")
     */
    public function backOfficeQuoteMail(Request $request, \Swift_Mailer $mailer) {
        $quote = new Quote();
        $form = $this->get("form.factory")->createNamed("sendmail",QuoteType::class, $quote);

        if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
            $quoteCategory = $form->get('quoteCategory')->getData();
            $em = $this->getDoctrine()->getManager();
            $allCommercial = $em->getRepository(Commercial::class)->findAllMail();
            $quote->setCreateAt(new \DateTime());
            $quote->setUserId($this->getUser());
            $em->persist($quote);
            $em->flush();
            // Generate PDF
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');

            $dompdf = new Dompdf($pdfOptions);
            $html = $this->renderView('templates/facture.html.twig', [
                'quote' => $quote,
                'quoteCategory' => $quoteCategory
            ]);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $output = $dompdf->output();

            $publicDirectory = $this->getParameter('kernel.project_dir').'/pdfs';
            $pdfFilepath =  $publicDirectory . '/facture'.$quote->getId().'.pdf';

            file_put_contents($pdfFilepath, $output);

            foreach($allCommercial as $key => $value)
            {
                $mail = (new \Swift_Message())
                    ->setSubject("Demande de devis")
                    ->setFrom(preg_replace("#\n|\t|\r#","", $value["mail_commercial"]))
                    ->setTo(preg_replace("#\n|\t|\r#","",$value["mail_commercial"]))
                    ->setBody("Merci de contacter le client rapidement depuis à cette adresse e-mail ". $quote->getMail(),
                        'text/html'
                    );
                $mail->attach(\Swift_Attachment::fromPath($pdfFilepath));
                $mailer->send($mail);
            }

            $mail = (new \Swift_Message())
                ->setSubject("Demande de devis")
                ->setFrom('testsiteg4@gmail.com')
                ->setTo($quote->getMail())
                ->setBody("Merci pour votre demande, notre commercial reviendra vers vous très prochainement.",
                    'text/html'
                );
            $mail->attach(\Swift_Attachment::fromPath($pdfFilepath));
            $mailer->send($mail);

            $this->get("session")->getFlashBag()->add("success", "Votre message a été envoyé avec succès");
            return $this->redirectToRoute("backOfficeMyQuote");
        }

        return $this->render('backOffice/quoteMail/index.html.twig', [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/quote/download/{id}", name="backOfficeQuoteDownload")
     */
    public function backOfficeQuoteDownload($id)
    {
        $path = $this->getParameter('kernel.project_dir').'/pdfs/facture'.$id.'.pdf';

        return $this->file($path);
    }

    /**
     * @Route("/admin/cart", name="backOfficeCart")
     */
    public function backOfficeCart()
    {
        return $this->render('cart/index.html.twig');
    }

    /**
     * @Route("/admin/cart/add{id}", name="backOfficeCartAdd")
     */
    public function backOfficeCartAdd($id, Request $request)
    {
        $session = $request->getSession();
        $cart = $session->get('cart', []);

        $panier[$id] = 1;

        $session->set('panier', $panier);

        dd($session->get('panier'));
    }

    /**
     * @Route("/admin/manage/user", name="backOfficeManageUser")
     */
    public function backOfficeManageUser(Request $request)
    {
        $users = $this->getDoctrine()->getManager()->getRepository(Users::class)->findAll();

        return $this->render('backOffice/manage/user.html.twig',[
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/manage/user/role/update/{id}", name="backOfficeManageUserRoleUpdate")
     */
    public function backOfficeManageUserRoleUpdate(Request $request, $id)
    {
        $users = $this->getDoctrine()->getManager()->getRepository(Users::class)->find($id);

        $form = $this->createFormBuilder()
            ->add('role', ChoiceType::class, [
            'label' => 'Role',
            'choices' => [
                'Utilisateur' => 'ROLE_USER',
                'Entreprise' => 'ROLE_COMPANY',
                'Administrateur' => 'ROLE_ADMIN'
            ]
        ])
            ->add('submit', SubmitType::class, [
                "label" => "Enregistrer"
            ])
            ->getForm()
        ;

        if($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $users->addRole($form->get('role')->getData());
            $em->persist($users);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le rôle vient d\'être ajouté");
            return $this->redirectToRoute("backOfficeManageUser");
        }

        return $this->render('backOffice/manage/roleUpdate.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @route("/admin/Commande/", name="backofficeCommande")
     */
    public function backofficeCommande() {
        $Commandes = $this->getDoctrine()->getManager()->getRepository(Commande::class)->findAll();

        return $this->render("backOffice/Commande/index.html.twig", [
            "commandes" => $Commandes,
        ]);
    }


    /**
     * @route("/admin/Commande/create", name="backofficeCommandeTypeCreate")
     */
    public function backofficeCommandeCreate(Request $request) {
        if ($this->getUser() === null) {
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $em = $this->getDoctrine()->getManager();
        $Commande = new \App\Entity\Commande();
        $form = $this->get("form.factory")->createNamed("createCommande", Commande::class, $Commande);

        if($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $Commande->setCreateBy($this->getUser());
            $em->persist($Commande);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type a été correctement créé");
            return $this->redirectToRoute("backofficeCommande");
        }

        return $this->render("backOffice/Commande/create.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @route("/admin/Commande/udpate/{id}", name="backofficeCommandeUpdate")
     */
    public function backofficeCommandeUpdate(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $Commande = $em->getRepository(Commande::class)->find($id);

        if($Commande !== null) {
            $form = $this->get("form.factory")->createNamed("updateCommande", CommandeType::class, $Commande);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {

                $em->persist($Commande);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Le type a été mise à jour");
                return $this->redirectToRoute("backofficeCommande");
            }

            return $this->render("backOffice/Commande/update.html.twig", [
                "form" => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/admin/Commande/delete/{id}", name="backofficeCommandeDelete")
     */
    public function backofficeCommandeDelete(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $Commande = $em->getRepository(\App\Entity\Commande::class)->find($id);

        if ($Commande !== null) {

            $em->remove($Commande);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type a été supprimé");
            return $this->redirectToRoute("backofficeCommande");
        }

        $this->get("session")->getFlashBag()->add("error", "Cette ressource n'existe pas !");
        return $this->redirectToRoute("backofficeCommande");
    }

    /**
     * @route("/admin/CommandeLine/", name="backofficeCommandeLine")
     */
    public function backofficeCommandeLine() {
        $CommandeLines = $this->getDoctrine()->getManager()->getRepository(CommandeLine::class)->findAll();

        return $this->render("backOffice/CommandeLine/index.html.twig", [
            "commandeLines" => $CommandeLines,
        ]);
    }

        /**
     * @route("/admin/CommandeLine/view", name="backofficeCommandeLineView")
     */
    public function backofficeCommandeLineView(Request $request) {
        $CommandeLines = $this->getDoctrine()->getManager()->getRepository(CommandeLine::class)->findbyIdCl($request->query->get('id'));

        return $this->render("backOffice/CommandeLine/view.html.twig", [
            "commandeLines" => $CommandeLines,
        ]);
    }


    /**
     * @route("/admin/CommandeLine/create", name="backofficeCommandeLineTypeCreate")
     */
    public function backofficeCommandeLineCreate(Request $request) {
        if ($this->getUser() === null) {
            $this->get("session")->getFlashBag()->add("error", "Merci de vous authentifier");
            return $this->redirectToRoute("fos_user_security_login");
        }

        $em = $this->getDoctrine()->getManager();
        $CommandeLine = new \App\Entity\CommandeLine();
        $form = $this->get("form.factory")->createNamed("createCommandeLine", CommandeLine::class, $CommandeLine);

        if($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $CommandeLine->setCreateBy($this->getUser());
            $em->persist($CommandeLine);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type a été correctement créé");
            return $this->redirectToRoute("backofficeCommandeLine");
        }

        return $this->render("backOffice/CommandeLine/create.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @route("/admin/CommandeLine/udpate/{id}", name="backofficeCommandeLineUpdate")
     */
    public function backofficeCommandeLineUpdate(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $CommandeLine = $em->getRepository(CommandeLine::class)->find($id);

        if($CommandeLine !== null) {
            $form = $this->get("form.factory")->createNamed("updateCommandeLine", CommandeLineType::class, $CommandeLine);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {

                $em->persist($CommandeLine);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Le type a été mise à jour");
                return $this->redirectToRoute("backofficeCommandeLine");
            }

            return $this->render("backOffice/CommandeLine/update.html.twig", [
                "form" => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/admin/CommandeLine/delete/{id}", name="backofficeCommandeLineDelete")
     */
    public function backofficeCommandeLineDelete(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $CommandeLine = $em->getRepository(\App\Entity\CommandeLine::class)->find($id);

        if ($CommandeLine !== null) {

            $em->remove($CommandeLine);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Le type a été supprimé");
            return $this->redirectToRoute("backofficeCommandeLine");
        }

        $this->get("session")->getFlashBag()->add("error", "Cette ressource n'existe pas !");
        return $this->redirectToRoute("backofficeCommandeLine");
    }

    /**
     * @route("/admin/user/view", name="backofficeUserView")
     */
    public function backofficeUserView() {
        $users = $this->getDoctrine()->getManager()->getRepository(Users::class)->findAll();

        return $this->render("backOffice/users/index.html.twig", [
            "users" => $users,
        ]);
    }


      /**
     * @Route("/admin/service", name="backOfficeService")
     */
    public function backOfficeService()
    {
        $allServices = $this->getDoctrine()->getManager()->getRepository(Service::class)->findAll();
        return $this->render('backOffice/service/index.html.twig', [
            'allServices' => $allServices,
        ]);
    }

    /**
     * @Route("/admin/service/create", name="backOfficeServiceCreate")
     */
    public function backOfficeServiceCreate(Request $request)
    {
        $service = new Service();
        $form = $this->get("form.factory")->createNamed("createService",ServiceType::class,$service);

        if($request->isMethod("post") && $form->handleRequest($request)->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
           // $service->setCreateBy($this->getUser());
            $em->persist($service);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Service créé avec succès");
            return $this->redirectToRoute("backOfficeService");
        }
            return $this->render('backOffice/service/create.html.twig', [
                "form" => $form->createView(),
            ]);
        
    }

    
    /**
     * @Route("/admin/service/update/{id}", name="backOfficeServiceUpdate")
     */
    public function backOfficeServiceUpdate(Request $request, $id){
        $service = $this->getDoctrine()->getManager()->getRepository(Service::class)->find($id);

        if($service !== null) {
            $form = $this->get("form.factory")->createNamed("updatedService",ServiceType::class, $service);

            if($request->isMethod("post") && $form->handleRequest($request)->isValid()) {
                //$service->setUpdateAt(new \DateTime());
                //$service->setUpdateBy($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($service);
                $em->flush();

                $this->get("session")->getFlashBag()->add("success", "Service modifé avec succès");
                return $this->redirectToRoute("backOfficeService");
            }

            return $this->render('backOffice/service/update.html.twig', [
                "form" => $form->createView(),
            ]);
        } else {
            $this->addFlash('error', 'La ressource n\'a pas été mise à jour car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeService');
        }
    }

    /**
     * @Route("/admin/service/delete/{id}", name="backOfficeServiceDelete")
     */
    public function backOfficeServiceDelete(Request $request, $id){
        $service = $this->getDoctrine()->getManager()->getRepository(Service::class)->find($id);

        if($service !== null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($service);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Service supprimé avec succès");

            return $this->redirectToRoute("backOfficeService");
        } else {
            $this->addFlash('error', 'Le service n\'a pas été supprimé car elle n\'existe pas');
            return $this->redirectToRoute('backOfficeSerivce');
        }
    }

}