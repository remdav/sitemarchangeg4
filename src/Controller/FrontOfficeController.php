<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Service;
use App\Entity\Product;
use App\Entity\Commande;
use App\Entity\CommandeLine;
use App\Entity\QuoteCategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FrontOfficeController extends AbstractController
{
    /**
     * @Route("/", name="frontOfficeIndex")
     */
    public function index()
    {
        $allProductCatalog = $this->getDoctrine()->getManager()->getRepository(Product::class)->findAll();

        return $this->render('frontOffice/index/index.html.twig', [
            'controller_name' => 'FrontOfficeController',
            "allProductCatalog" => $allProductCatalog,
        ]);
    }

    /**
     * @Route("/product", name="frontOfficeProductCatalog")
     */
    public function productCatalog()
    {
        $allProductCatalog = $this->getDoctrine()->getManager()->getRepository(Product::class)->findAll();

        return $this->render('frontOffice/product/index.html.twig', [
            "allProductCatalog" => $allProductCatalog,
        ]);
    }

    /**
     * @Route("/best-sale", name="frontOfficeBestSale")
     */
    public function bestSale()
    {
        return $this->render('frontOffice/bestSale/view.html.twig', [
            'controller_name' => 'FrontOfficeController',
        ]);
    }

    /**
     * @Route("/article", name="frontOfficeArticle")
     */
    public function frontOfficeArticle()
    {
        $allArticles = $this->getDoctrine()->getManager()->getRepository(Articles::class)->findAll();

        return $this->render('frontOffice/article/index.html.twig', [
            "allArticles" => $allArticles,
        ]);
    }

    /**
     * @Route("/article/{slug}", name="frontOfficeArticleBySlug")
     */
    public function frontOfficeArticleBySlug($slug)
    {
        $article = $this->getDoctrine()->getManager()->getRepository(Articles::class)->findBySlug($slug);

        return $this->render('frontOffice/article/article.html.twig', [
            "article" => $article,
        ]);
    }

 /**
     * @Route("/service", name="frontOfficeService")
     */
    public function frontOfficeService()
    {
        $allQuotes = $this->getDoctrine()->getManager()->getRepository(QuoteCategory::class)->findAll();

        return $this->render('frontOffice/quote/index.html.twig', [
            'allQuotes' => $allQuotes,
        ]);
    }
    
 /**
     * @Route("/services", name="frontOfficeServices")
     */
    public function frontOfficeServices()
    {
        $allServices = $this->getDoctrine()->getManager()->getRepository(Service::class)->findAll();

        return $this->render('frontOffice/service/index.html.twig', [
            'allServices' => $allServices,
        ]);
    }
    

    /**
     * @Route("/product/view/{id}", name="frontOfficeProduct")
     */
    public function frontOfficeProduct($id)
    {
        $products = $this->getDoctrine()->getManager()->getRepository(Product::class)->find($id);

        return $this->render('frontOffice/product/view.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/zeroDechet", name="frontOfficeZeroDechet")
     */
    public function frontOfficeZeroDechet()
    {
     //  $products = $this->getDoctrine()->getManager()->getRepository(Product::class)->find();

        return $this->render('frontOffice/zeroDechet/index.html.twig');

    }

    /**
     * @Route("/cart", name="frontOfficeCart")
     */
    public function frontOfficeCart()
    {
        if (isset($_SESSION['card']))
        {
            $total=0;
            for($i = 0; $i < count($_SESSION['card']['produit']); $i++)
            {
               $total += $_SESSION['card']['total'][$i];
            }

            if(!isset($_SESSION['card']['totalOrder']))
            {
                array_push($_SESSION['card']['totalOrder'] ,$total);
            }
            else
            {
                $_SESSION['card']['totalOrder'][0] = $total;
            }

            return $this->render('frontOffice/cart/index.html.twig' , [
            "card" => $_SESSION['card']

            ]);
        }
        else return $this->index();
    }

    /**
     * @Route("/topTen", name="frontOfficeTopTen")
     */
    public function backOfficeTopTen()
    {
        $allTopTen = $this->getDoctrine()->getManager()->getRepository(Product::class)->findAllTopTenCommande();

        return $this->render('frontOffice/TopTen/index.html.twig', [
            "allTopTen" => $allTopTen,
        ]);
    }

    /**
     * @Route("/contact", name="frontOfficeContact")
     */
    public function frontOfficeContact(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createFormBuilder()
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom'
            ])
            ->add('phone', IntegerType::class, [
                'label' => 'Numéro de téléphone'
            ])
            ->add('mail', EmailType::class, [
                'label' => 'Adresse mail'
            ])
            ->add('object', TextType::class, [
                'label' => 'Objet de la demande'
            ])
            ->add('describe', TextareaType::class, [
                'label' => 'Description de la demande'
            ])
            ->add('submit', SubmitType::class, [
                "label" => "Envoyer"
            ])
            ->getForm()
        ;

        if($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $mail = (new \Swift_Message())
                ->setSubject($form->get('object')->getData())
                ->setFrom($form->get('mail')->getData())
                ->setTo('testsiteg4@gmail.com')
                ->setBody("Bonjour, une nouvelle demande de ".$form->get('name')->getData()." ".$form->get('firstName')->getData()." vient d'arriver.<br /> Voici l'objet de la demande: ".$form->get('object')->getData().". <br />La demande complète est la suivante : <br /> ".$form->get('describe')->getData()."<br /> Vous pouvez contacter ce client par mail à cette adresse : ".$form->get('mail')->getData()."<br />Ou par téléphone à ce numéro ".$form->get('phone')->getData()."<br />Merci de le contacter le plus rapidement possible.<br />Cordialement.",
                    'text/html'
                );
            $mailer->send($mail);

            $this->get("session")->getFlashBag()->add("success", "Votre email a été envoyé avec succès. Nous reviendrons vers vous le plus vite possible");
        }

        return $this->render('frontOffice/contact/index.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/test", name="frontOfficetest")
     */
    public function frontOfficeCreateCard()
    {
        if (!isset($_SESSION['card'])){
            $_SESSION['card']=array();
            $_SESSION['card']['produit'] = array();
            $_SESSION['card']['name']= array();
            $_SESSION['card']['qty'] = array();
            $_SESSION['card']['price'] = array();
            $_SESSION['card']['total'] = array();
            $_SESSION['card']['totalOrder'] = array();
            $_SESSION['card']['lock'] = false;
        }

        return true;
    }

    /**
     * @Route("/clear", name="frontOfficetest")
     */
    public function frontOfficeTest()
    {
        unset($_SESSION['card']);
        return $this->ProductCatalog();
    }

    /**
     * @Route("/add/{id}/{qty}", name="frontOfficeAddCard")
     */
    public function frontOfficeAddCard($id,$qty)
    {
        if ($this->frontOfficeCreateCard() && !$_SESSION['card']['lock'])
        {
            $qty = intval($qty);
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository(\App\Entity\Product::class)->find($id);

           //Si le produit existe déjà on ajoute seulement la quantité
           $positionProduit = array_search($id,  $_SESSION['card']['produit']);
           if ($positionProduit !== false)
           {
                 $_SESSION['card']['qty'][$positionProduit] += $qty ;
                 $_SESSION['card']['total'][$positionProduit] += $qty * $_SESSION['card']['price'][$positionProduit] ;
           }
           else
           {
              //Sinon on ajoute le produit
              array_push( $_SESSION['card']['produit'],$id);
              array_push( $_SESSION['card']['qty'],$qty);
              array_push( $_SESSION['card']['price'],$product->getPrice() );
              array_push( $_SESSION['card']['total'],$product->getPrice() * $qty );
              array_push(  $_SESSION['card']['name'],$product->getName());
           }
        }
        else
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
        return $this->ProductCatalog();

     }


    /**
     * @Route("/delete/{id}", name="frontOfficeDeleteCard")
     */
    public function frontOfficeDeleteCard($id)
    {
          //Si le panier existe
        if ($this->frontOfficeCreateCard() && !$_SESSION['card']['lock'])
        {
      //Nous allons passer par un panier temporaire
            $tmp=array();
            $tmp['produit'] = array();
            $tmp['name']= array();
            $tmp['qty'] = array();
            $tmp['price'] = array();
            $tmp['total'] = array();
            $tmp['totalOrder'] = array();
            $tmp['lock'] = $_SESSION['card']['lock'];

            for($i = 0; $i < count($_SESSION['card']['produit']); $i++)
            {
                if ($_SESSION['card']['produit'][$i] !== $id)
                {
                    array_push( $tmp['produit'] ,$_SESSION['card']['produit'][$i]);
                    array_push( $tmp['name'], $_SESSION['card']['name'][$i]);
                    array_push( $tmp['qty'],$_SESSION['card']['qty'][$i]);
                    array_push( $tmp['price'],$_SESSION['card']['price'][$i]);
                    array_push( $tmp['total'],$_SESSION['card']['total'][$i]);
                    array_push( $tmp['totalOrder'],$_SESSION['card']['totalOrder'][0]);

                    //array_push( $tmp['client'] ,$_SESSION['card']['client'][$i]);

                }

            }
           // array_push( $tmp['lock'] ,$_SESSION['card']['lock']);

            //On remplace le panier en session par notre panier temporaire à jour
            $_SESSION['card'] =  $tmp;
            //On efface notre panier temporaire
            unset($tmp);
        }
        else
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
        return $this-> frontOfficeCart();

    }

    /**
     * @Route("/update/{id}/{qty}", name="frontOfficeUpdateCard")
     */
    public function frontOfficeUpdateCard($id,$qty)
    {
        if($this->frontOfficeCreateCard() && !$_SESSION['card']['lock'])
        {
            $qty = intval($qty);
           //Si la quantité est positive on modifie sinon on supprime l'article
           if ($qty > 0)
           {
              //Recharche du produit dans le panier
              $positionProduit = array_search($id, $_SESSION['card']['produit']);
              //dump($positionProduit);
              //dump($id);
              if ($positionProduit !== false)
              {
                 $_SESSION['card']['qty'][$positionProduit] = $qty ;
                 $_SESSION['card']['total'][$positionProduit] = $qty * $_SESSION['card']['price'][$positionProduit] ;
              }
           }
           else
           $this->frontOfficeDeleteCard($id);
        }
        else
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
        return $this-> frontOfficeCart();
    }

    /**
     * @Route("/insert", name="frontOfficeInsert")
     */
    public function frontOfficeInsert()
    {
        if($this->frontOfficeCreateCard() && !$_SESSION['card']['lock'])
        {
                $commande = new Commande();
                $commandeLine = new CommandeLine();


                $em = $this->getDoctrine()->getManager();

                $commande->setStatus(true);
                $commande->setCreateAt(new \DateTime());
                $commande->setUserId($this->getUser());
                $em->persist($commande);
                $em->flush();

                for($i = 0; $i < count($_SESSION['card']['produit']); $i++)
                {
                    $commandeLine = new CommandeLine();
                    $product = $this->getDoctrine()->getManager()->getRepository(Product::class)->find($_SESSION['card']['produit'][$i]);
                    $commandeLine->setCreateAt(new \DateTime());
                    $commandeLine->setProductId($product);
                    $commandeLine->setQuantity($_SESSION['card']['qty'][$i]);
                    $commandeLine->setCommandeId($commande);
                    $em->persist($commandeLine);
                    $em->flush();

                }
                $this->get("session")->getFlashBag()->add("notice", "Le type de catégorie a été créé");
        }
        return $this->frontOfficeTest();


    }
}

