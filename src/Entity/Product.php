<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

    /**
     * @ORM\Column(type="text")
     */
    private $descriptive;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Type_Product_Id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $create_by;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     */
    private $update_by;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     */
    private $pictureName;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="image", fileNameProperty="pictureName")
     *
     */
    private $picture;

    public function __construct()
    {
        $this->setCreateAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getDescriptive(): ?string
    {
        return $this->descriptive;
    }

    public function setDescriptive(string $descriptive): self
    {
        $this->descriptive = $descriptive;

        return $this;
    }

    public function getTypeProductId(): ?ProductType
    {
        return $this->Type_Product_Id;
    }

    public function setTypeProductId(?ProductType $Type_Product_Id): self
    {
        $this->Type_Product_Id = $Type_Product_Id;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getCreateBy(): ?Users
    {
        return $this->create_by;
    }

    public function setCreateBy(?Users $create_by): self
    {
        $this->create_by = $create_by;

        return $this;
    }

    public function getUpdateBy(): ?Users
    {
        return $this->update_by;
    }

    public function setUpdateBy(?Users $update_by): self
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getPictureName()
    {
        return $this->pictureName;
    }

    /**
     * @param string $pictureName
     * @return Product
     */
    public function setPictureName($pictureName)
    {
        $this->pictureName = $pictureName;
        return $this;
    }

    /**
     * @return File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param File $picture
     * @return Product
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }
}
