<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuoteCategoryRepository")
 */
class QuoteCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $udpate_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $create_by;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     */
    private $update_by;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Commercial", mappedBy="QuoteCat")
     */
    private $commercials;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quote", inversedBy="quoteCategories")
     */
    private $QuoteId;

    public function __construct()
    {
        $this->user_id = new ArrayCollection();
        $this->commercials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUdpateAt(): ?\DateTimeInterface
    {
        return $this->udpate_at;
    }

    public function setUdpateAt(?\DateTimeInterface $udpate_at): self
    {
        $this->udpate_at = $udpate_at;

        return $this;
    }

    public function getCreateBy(): ?Users
    {
        return $this->create_by;
    }

    public function setCreateBy(?Users $create_by): self
    {
        $this->create_by = $create_by;

        return $this;
    }

    public function getUpdateBy(): ?Users
    {
        return $this->update_by;
    }

    public function setUpdateBy(?Users $update_by): self
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * @return Collection|Commercial[]
     */
    public function getCommercials(): Collection
    {
        return $this->commercials;
    }

    public function addCommercial(Commercial $commercial): self
    {
        if (!$this->commercials->contains($commercial)) {
            $this->commercials[] = $commercial;
            $commercial->addQuoteCat($this);
        }

        return $this;
    }

    public function removeCommercial(Commercial $commercial): self
    {
        if ($this->commercials->contains($commercial)) {
            $this->commercials->removeElement($commercial);
            $commercial->removeQuoteCat($this);
        }

        return $this;
    }

    public function getQuoteId(): ?Quote
    {
        return $this->QuoteId;
    }

    public function setQuoteId(?Quote $QuoteId): self
    {
        $this->QuoteId = $QuoteId;

        return $this;
    }
}
