<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommercialRepository")
 */
class Commercial
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_commercial;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $numero_commercial;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail_commercial;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\QuoteCategory", inversedBy="commercials")
     */
    private $QuoteCat;

    public function __construct()
    {
        $this->QuoteCat = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCommercial(): ?string
    {
        return $this->nom_commercial;
    }

    public function setNomCommercial(string $nom_commercial): self
    {
        $this->nom_commercial = $nom_commercial;

        return $this;
    }

    public function getNumeroCommercial(): ?string
    {
        return $this->numero_commercial;
    }

    public function setNumeroCommercial(string $numero_commercial): self
    {
        $this->numero_commercial = $numero_commercial;

        return $this;
    }

    public function getMailCommercial(): ?string
    {
        return $this->mail_commercial;
    }

    public function setMailCommercial(string $mail_commercial): self
    {
        $this->mail_commercial = $mail_commercial;

        return $this;
    }

    /**
     * @return Collection|QuoteCategory[]
     */
    public function getQuoteCat(): Collection
    {
        return $this->QuoteCat;
    }

    public function addQuoteCat(QuoteCategory $quoteCat): self
    {
        if (!$this->QuoteCat->contains($quoteCat)) {
            $this->QuoteCat[] = $quoteCat;
        }

        return $this;
    }

    public function removeQuoteCat(QuoteCategory $quoteCat): self
    {
        if ($this->QuoteCat->contains($quoteCat)) {
            $this->QuoteCat->removeElement($quoteCat);
        }

        return $this;
    }
}
