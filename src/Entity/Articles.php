<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticlesRepository")
 * @Vich\Uploadable
 */
class Articles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     */
    private $create_by;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     */
    private $Update_by;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     */
    private $pictureName;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="image", fileNameProperty="pictureName")
     *
     */
    private $picture;

    public function __construct()
    {
        $this->setCreateAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getCreateBy(): ?Users
    {
        return $this->create_by;
    }

    public function setCreateBy(?Users $create_by): self
    {
        $this->create_by = $create_by;

        return $this;
    }

    public function getUpdateBy(): ?Users
    {
        return $this->Update_by;
    }

    public function setUpdateBy(?Users $Update_by): self
    {
        $this->Update_by = $Update_by;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getPictureName()
    {
        return $this->pictureName;
    }

    /**
     * @param string $pictureName
     * @return Articles
     */
    public function setPictureName($pictureName)
    {
        $this->pictureName = $pictureName;
        return $this;
    }

    /**
     * @return File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param File $picture
     * @return Articles
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }
}
