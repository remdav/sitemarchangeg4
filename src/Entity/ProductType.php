<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductTypeRepository")
 */
class ProductType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $denominated;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $create_by;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDenominated(): ?string
    {
        return $this->denominated;
    }

    public function setDenominated(string $denominated): self
    {
        $this->denominated = $denominated;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreateBy(): ?Users
    {
        return $this->create_by;
    }

    public function setCreateBy(?Users $create_by): self
    {
        $this->create_by = $create_by;

        return $this;
    }
}
